﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace SzklanaKula
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int czas = 10;
        int zapis = Int32.Parse(System.IO.File.ReadAllText("os.txt"));

        private void Form1_Load(object sender, EventArgs e)
        {
            //Ustawia program na full screen i wyśrodkuje wszystko
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;

            int fWidth = this.Width;
            int fHeight = this.Height;

            Title.Location = new Point((fWidth - Title.Width) / 2, (fHeight - Title.Height) / 6);
            button1.Location = new Point((fWidth - button1.Width) / 2, (fHeight - button1.Height) - 50);
            button5.Location = new Point(fWidth - button5.Width, fHeight - button5.Height);

            //Zmienne.Osoba = Zmienne.Osoba + 1;
            Zmienne.Osoba = zapis;
            Zapis.Start = true;
            
            if (Zmienne.Osoba == 4)
            {
                button1.Hide();
                label1.Show();
                label1.Text = "";
                Title.Text = "";
                label1.Location = new Point((fWidth - label1.Width) / 2, (fHeight - label1.Height) / 2);
            }
        }

        private void Imie()
        {
            string imiona = @"imiona.txt";

            int fWidth = this.Width;
            int fHeight = this.Height;

            //Random process
            Random rnd = new Random();
            //Count how much there's lines in splash files
            int count = System.IO.File.ReadLines(imiona).Count();
            //generates number from 1 to number of lines in splashes.ini file
            int number = rnd.Next(0, count);
            //Skip NUMBER lines and get it to string
            string text = System.IO.File.ReadLines(imiona, System.Text.Encoding.UTF8).Skip(number).Take(1).First();
            //get string to line
            label1.Text = text;

            label1.Location = new Point((fWidth - label1.Width) / 2, (fHeight - label1.Height) / 2);
        }

        private Timer timer1;
        public void InitTimer()
        {
            timer1 = new Timer();
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = czas;
            timer1.Start();
        }

        public void timer1_Tick(object sender, EventArgs e)
        {
            int fWidth = this.Width;
            int fHeight = this.Height;

            if (czas >= 500)
            {
                timer1.Interval = 999999;
                timer2.Interval = 999999;
                
                if (Zmienne.Osoba == 1)
                {
                    label1.Text = "Kacper Kuliński";
                    label1.Location = new Point((fWidth - label1.Width) / 2, (fHeight - label1.Height) / 2);
                }
                else if (Zmienne.Osoba == 2)
                {
                    label1.Text = "Kacper Urbański";
                    label1.Location = new Point((fWidth - label1.Width) / 2, (fHeight - label1.Height) / 2);
                }
                else if (Zmienne.Osoba == 3)
                {
                    label1.Text = "Zuzanna Chała";
                    label1.Location = new Point((fWidth - label1.Width) / 2, (fHeight - label1.Height) / 2);
                }
                InitTimer3();
            }
            else
            {
                Imie();
            }
        }

        private Timer timer2;
        public void InitTimer2()
        {
            timer2 = new Timer();
            timer2.Tick += new EventHandler(timer2_Tick);
            timer2.Interval = 1000;
            timer2.Start();
        }

        public void timer2_Tick(object sender, EventArgs e)
        {
            czas = czas + 50;
            timer1.Interval = czas;
        }

        int etap = 1;

        private Timer timer3;
        public void InitTimer3()
        {
            timer3 = new Timer();
            timer3.Tick += new EventHandler(timer3_Tick);
            timer3.Interval = 3000;
            timer3.Start();
        }

        public void timer3_Tick(object sender, EventArgs e)
        {
            int fWidth = this.Width;
            int fHeight = this.Height;

            if (etap == 1)
            {
                label1.Text = label1.Text + " zostanie...";
                label1.Location = new Point((fWidth - label1.Width) / 2, (fHeight - label1.Height) / 2);
            }
            if (etap == 2)
            {
                if (Zmienne.Osoba == 1)
                {
                    label1.Text = "...kosmonautą!";
                    this.BackgroundImage = SzklanaKula.Properties.Resources.kac1_800;
                }
                if (Zmienne.Osoba == 2)
                {
                    label1.Text = "...raperem!";
                    this.BackgroundImage = SzklanaKula.Properties.Resources.kac2_800;
                }
                if (Zmienne.Osoba == 3)
                {
                    label1.Text = "...tancerką!";
                    this.BackgroundImage = SzklanaKula.Properties.Resources.zuz1_800;
                }

                this.label1.Font = new Font("Microsoft Sans Serif", 45, FontStyle.Regular);
                label1.Location = new Point((fWidth - label1.Width) / 2, (fHeight - label1.Height) - 10);
            }
            if (etap == 3)
            {
                File.WriteAllText("os.txt", (zapis + 1).ToString());
                Cursor.Show();
                button5.Visible = true;
            }

            if (etap < 3)
            {
                etap++;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Stream str = Properties.Resources.drums;
            System.Media.SoundPlayer snd = new System.Media.SoundPlayer(str);
            snd.Play();

            Title.Hide();
            Cursor.Hide();
            button1.Hide();
            label1.Visible = true;
            //Tekst     
            Imie();
            InitTimer();
            InitTimer2();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Zmienne.Osoba.ToString());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show(timer1.Interval.ToString());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show(etap.ToString());
        }

        private void button5_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("SzklanaKula.exe");
            Application.Exit();
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.Ke)
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1.PerformClick();
            }
        }
    }
}
